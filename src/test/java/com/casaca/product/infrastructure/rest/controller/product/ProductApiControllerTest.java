package com.casaca.product.infrastructure.rest.controller.product;

import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetail;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@ExtendWith(MockitoExtension.class)
@DisplayName("Product API tests")
@SpringBootTest
@AutoConfigureMockMvc
class ProductApiControllerTest {

	@Autowired
	protected MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	@DisplayName("Test: Given id 1. Ids 2, 3 and 4 will be recommended.")
	void given_an_id_product_when_call_the_external_api_to_find_the_similar_products_then_the_products_are_returned() throws Exception {

		//WHEN
		MockHttpServletResponse response = this.mockMvc.perform(MockMvcRequestBuilders
				.get("/product/1/similar")
				.accept(MediaType.APPLICATION_JSON))
			.andReturn().getResponse();

		//THEN
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		List<ProductDetail> body = this.objectMapper.readValue(
			response.getContentAsString(),
			new TypeReference<>() {
			}
		);

		assertEquals(3, body.size());
		assertEquals("2", body.get(0).getId());
		assertEquals("3", body.get(1).getId());
		assertEquals("4", body.get(2).getId());
	}

	@Test
	@DisplayName("Test: Given the id 50 has no recommendations")
	void given_a_product_id_when_it_has_not_similar_then_we_return_an_not_found() throws Exception {

		//WHEN
		MockHttpServletResponse response = this.mockMvc.perform(MockMvcRequestBuilders
				.get("/product/50/similar")
				.accept(MediaType.APPLICATION_JSON))
			.andReturn().getResponse();

		//THEN
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());

	}

	@Test
	@DisplayName("Test: Given product id 5, 3 products are recommended but one product throws an error and cannot be returned.")
	void given_a_product_id_when_finding_similar_ones_if_one_cannot_be_recovered_then_it_is_discarded() throws Exception {

		//WHEN
		MockHttpServletResponse response = this.mockMvc.perform(MockMvcRequestBuilders
				.get("/product/5/similar")
				.accept(MediaType.APPLICATION_JSON))
			.andReturn().getResponse();

		//THEN
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		List<ProductDetail> body = this.objectMapper.readValue(
			response.getContentAsString(),
			new TypeReference<>() {
			}
		);

		assertEquals(2, body.size());

	}
	@Test
	@DisplayName("Test: Given product id 2, 3 products are recommended but one product has a delay.")
	void given_a_product_id_when_calling_the_external_api_and_it_has_a_delay_then_an_empty_collection_is_returned() throws Exception {

		//WHEN
		MockHttpServletResponse response = this.mockMvc.perform(MockMvcRequestBuilders
				.get("/product/2/similar")
				.accept(MediaType.APPLICATION_JSON))
			.andReturn().getResponse();

		//THEN
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		List<ProductDetail> body = this.objectMapper.readValue(
			response.getContentAsString(),
			new TypeReference<>() {
			}
		);

		assertEquals(0, body.size());

	}
}