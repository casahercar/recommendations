package com.casaca.product.mock;

import com.casaca.product.domain.model.Product;
import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetail;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class Mocks {

	public static Product productMock(int index) {
		Product productMock = new Product();
		productMock.setId(String.valueOf(index));
		productMock.setName("::name::" + index);
		productMock.setPrice(BigDecimal.valueOf(index * 5L));
		productMock.setAvailability(index % 2 == 0);
		return productMock;
	}

	public static ProductDetail productDetailMock(int index) {
		return new ProductDetail()
			.setId(String.valueOf(index * 5))
			.setName("::name::" + index)
			.setPrice(BigDecimal.valueOf(index * 4L))
			.setAvailability(index % 2 == 0);
	}

	public static List<String> similarProductIds(int index) {
		ArrayList<String> similarProductIds = new ArrayList<>();

		similarProductIds.add(String.valueOf(index + 5));
		similarProductIds.add(String.valueOf(index + 2));
		similarProductIds.add(String.valueOf(index + 8));
		return similarProductIds;
	}
}
