package com.casaca.product.application;

import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetail;
import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetailMapper;
import com.casaca.product.infrastructure.rest.controller.product.response.ProductsDetailResponseMapper;
import com.casaca.product.mock.Mocks;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductsDetailResponseMapperTest {


	@InjectMocks
	private ProductsDetailResponseMapper mapper;

	@Mock
	private ProductDetailMapper productDetailMapper;


	@Test
	void when_mapper_products_list_is_used_then_product_detail_response_mapper_is_used() {
		this.mapper.map(List.of(
				Mocks.productMock(2),
				Mocks.productMock(3)
			)
		);

		verify(this.productDetailMapper).map(Mocks.productMock(2));
		verify(this.productDetailMapper).map(Mocks.productMock(3));
	}

	@Test
	void when_mapper_products_list_is_used_then_info_is_retrieved() {
		when(this.productDetailMapper.map(any()))
			.thenReturn(Mocks.productDetailMock(5));


		List<ProductDetail> productDetails = this.mapper.map(List.of(
				Mocks.productMock(1),
				Mocks.productMock(2)
			)
		);

		assertEquals(List.of(
			Mocks.productDetailMock(5),
			Mocks.productDetailMock(5)
		), productDetails);
	}
}