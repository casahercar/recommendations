package com.casaca.product.application.service.cache;

import com.casaca.product.domain.model.Product;
import com.casaca.product.mock.Mocks;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class SimilarProductsCacheTest {
	@InjectMocks
	private SimilarProductsCache cache;

	@Test
	void when_product_id_and_a_list_of_products_is_received_then_it_is_added_to_the_cache_map() {
		List<Product> products = List.of(Mocks.productMock(1));
		cache.put("1", products);
		List<Product> result = cache.get("1");

		Assertions.assertNotNull(result);
		assertEquals(result, products);
	}
}