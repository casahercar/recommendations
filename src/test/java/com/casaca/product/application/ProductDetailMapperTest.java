package com.casaca.product.application;

import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetail;
import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetailMapper;
import com.casaca.product.mock.Mocks;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class ProductDetailMapperTest {

	@InjectMocks
	private ProductDetailMapper mapper;


	@Test
	void when_mapper_is_used_then_info_is_returned() {
		ProductDetail productDetail = this.mapper.map(
				Mocks.productMock(5))
			.setId("1")
			.setName("::name::")
			.setPrice(BigDecimal.valueOf(20.0))
			.setAvailability(Boolean.TRUE);

		assertEquals("1", productDetail.getId());
		assertEquals("::name::", productDetail.getName());
		assertEquals(BigDecimal.valueOf(20.0), productDetail.getPrice());
		assertEquals(Boolean.TRUE, productDetail.getAvailability());
	}
}