package com.casaca.product.application;


import com.casaca.product.application.service.cache.SimilarProductsCache;
import com.casaca.product.application.service.product.RecommendedProductsService;
import com.casaca.product.domain.model.Product;
import com.casaca.product.domain.repository.RecommendedProductRepository;
import com.casaca.product.mock.Mocks;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecommendedProductServiceTest {

	private RecommendedProductsService recommendedProductsService;
	@Mock
	private RecommendedProductRepository recommendedProductRepository;
	@Mock
	private SimilarProductsCache similarProductsCache;


	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
		recommendedProductsService = new RecommendedProductsService(
			this.recommendedProductRepository,
			this.similarProductsCache
		);
	}

	@Test
	void when_get_similar_product_not_have_any_similar_products_then_empty_response() {
		when(this.similarProductsCache.get(anyString())).thenReturn(null);
		when(this.recommendedProductRepository.getProducts(anyString())).thenReturn(List.of());

		List<Product> similarProducts = this.recommendedProductsService.execute("1");

		assertTrue(similarProducts.isEmpty());

	}

	@Test
	void when_similar_products_not_have_any_product_then_empty_list() {
		when(this.similarProductsCache.get(anyString())).thenReturn(null);
		when(this.recommendedProductRepository.getProducts(anyString())).thenReturn(List.of());

		List<Product> result = this.recommendedProductsService.execute("1");

		assertTrue(result.isEmpty());
	}

	@Test
	void when_get_similar_products_success_then_result_is_ok() {
		when(this.similarProductsCache.get(anyString())).thenReturn(null);
		when(this.recommendedProductRepository.getProducts(anyString())).thenReturn(
			List.of(Mocks.productMock(1), Mocks.productMock(2), Mocks.productMock(3)));

		List<Product> result = this.recommendedProductsService.execute("1");

		assertThat(result).isNotEmpty();
	}

	@Test
	void when_there_are_products_in_the_cache_then_the_cached_list_is_returned() {
		when(this.similarProductsCache.get(anyString())).thenReturn(List.of(
			Mocks.productMock(1), Mocks.productMock(2)));

		List<Product> products = this.recommendedProductsService.execute("1");

		assertEquals(List.of(
			Mocks.productMock(1), Mocks.productMock(2)), products);

	}
}