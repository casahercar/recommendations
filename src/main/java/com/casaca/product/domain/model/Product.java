package com.casaca.product.domain.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Product {

	private String id;

	private String name;

	private BigDecimal price;

	private Boolean availability;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getAvailability() {
		return availability;
	}

	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;
		Product product = (Product)o;
		return Objects.equals(id, product.id) && Objects.equals(
			name,
			product.name
		) && Objects.equals(price, product.price) && Objects.equals(availability, product.availability);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, price, availability);
	}
}
