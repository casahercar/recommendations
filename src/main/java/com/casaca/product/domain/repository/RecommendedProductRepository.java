package com.casaca.product.domain.repository;

import com.casaca.product.domain.model.Product;
import java.util.List;

public interface RecommendedProductRepository {

	List<Product> getProducts(String productId);


}
