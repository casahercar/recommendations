package com.casaca.product.application.service.product;

import com.casaca.product.application.service.cache.SimilarProductsCache;
import com.casaca.product.domain.model.Product;
import com.casaca.product.domain.repository.RecommendedProductRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RecommendedProductsService {

	private final RecommendedProductRepository recommendedProductRepository;
	private final SimilarProductsCache similarProductsCache;

	public List<Product> execute(String productId) {
		List<Product> cachedProducts = similarProductsCache.get(productId);
		if(cachedProducts != null) {
			return cachedProducts;
		}

		return this.recommendedProductRepository.getProducts(productId);

	}

}
