package com.casaca.product.application.service.cache;

import com.casaca.product.domain.model.Product;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SimilarProductsCache {
	private final Map<String, List<Product>> cache = new ConcurrentHashMap<>();

	public void put(String productId, List<Product> products) {
		cache.put(productId, products);
	}

	public List<Product> get(String productId) {
		return cache.get(productId);
	}
}

