package com.casaca.product.application.exceptions;

public class NotFoundSimilarProductsException extends RuntimeException {
	public NotFoundSimilarProductsException(String message) {
		super(message);
	}
}
