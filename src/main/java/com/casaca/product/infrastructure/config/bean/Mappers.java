package com.casaca.product.infrastructure.config.bean;

import com.casaca.product.infrastructure.rest.api.product.get.ProductResponseApiMapper;
import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetailMapper;
import com.casaca.product.infrastructure.rest.controller.product.response.ProductsDetailResponseMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Mappers {

	@Bean
	public ProductResponseApiMapper productResponseApiMapper() {
		return new ProductResponseApiMapper();
	}

	@Bean
	public ProductDetailMapper productDetailMapper() {
		return new ProductDetailMapper();
	}

	@Bean
	public ProductsDetailResponseMapper productsDetailResponseMapper(ProductDetailMapper productDetailMapper) {
		return new ProductsDetailResponseMapper(productDetailMapper);
	}


}
