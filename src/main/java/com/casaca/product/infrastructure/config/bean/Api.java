package com.casaca.product.infrastructure.config.bean;

import com.casaca.product.application.service.cache.SimilarProductsCache;
import com.casaca.product.domain.repository.RecommendedProductRepository;
import com.casaca.product.infrastructure.rest.api.product.get.ProductResponseApiMapper;
import com.casaca.product.infrastructure.rest.api.product.repository.RecommendedProductApi;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class Api {

	@Bean
	public RecommendedProductRepository productRepository(
		SimilarProductsCache similarProductsCache,
		ProductResponseApiMapper productResponseApiMapper
	) {
		return new RecommendedProductApi(similarProductsCache, productResponseApiMapper);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplateBuilder()
			.build();
	}
}
