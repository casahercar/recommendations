package com.casaca.product.infrastructure.config.bean;

import com.casaca.product.application.service.cache.SimilarProductsCache;
import com.casaca.product.application.service.product.RecommendedProductsService;
import com.casaca.product.domain.repository.RecommendedProductRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Services {

	@Bean
	public SimilarProductsCache similarProductsCache() {
		return new SimilarProductsCache();
	}

	@Bean()
	public RecommendedProductsService recommendedProductsService(
		RecommendedProductRepository recommendedProductRepository,
		SimilarProductsCache similarProductCache
	) {
		return new RecommendedProductsService(recommendedProductRepository, similarProductCache);
	}
}
