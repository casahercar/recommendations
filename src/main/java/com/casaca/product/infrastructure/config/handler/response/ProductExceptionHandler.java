package com.casaca.product.infrastructure.config.handler.response;

import com.casaca.product.application.exceptions.NotFoundSimilarProductsException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@RequiredArgsConstructor
public class ProductExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NotFoundSimilarProductsException.class)
	public ResponseEntity<ErrorResponse> handlerNotFoundSimilarProductException(NotFoundSimilarProductsException ex) {
		ErrorResponse errorResponse = new ErrorResponse(ex.getMessage(), HttpStatus.NOT_FOUND.toString());
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

}
