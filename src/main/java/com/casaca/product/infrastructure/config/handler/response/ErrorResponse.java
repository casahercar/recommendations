package com.casaca.product.infrastructure.config.handler.response;

import lombok.Data;

@Data
public class ErrorResponse {
	private final String message;
	private final String httpStatusCode;
}
