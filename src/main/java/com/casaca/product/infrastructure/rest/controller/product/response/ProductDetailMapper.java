package com.casaca.product.infrastructure.rest.controller.product.response;

import com.casaca.product.domain.model.Product;

public class ProductDetailMapper {

	public ProductDetail map(Product product) {
		return new ProductDetail()
			.setId(product.getId())
			.setName(product.getName())
			.setPrice(product.getPrice())
			.setAvailability(product.getAvailability());

	}
}
