package com.casaca.product.infrastructure.rest.controller.product.response;

import com.casaca.product.domain.model.Product;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProductsDetailResponseMapper {

	private final ProductDetailMapper productDetailMapper;

	public List<ProductDetail> map(List<Product> products) {

		return products.stream()
			.map(this.productDetailMapper::map)
			.collect(Collectors.toList());

	}
}
