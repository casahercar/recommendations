package com.casaca.product.infrastructure.rest.api.product.repository;

import com.casaca.product.application.exceptions.NotFoundSimilarProductsException;
import com.casaca.product.application.service.cache.SimilarProductsCache;
import com.casaca.product.domain.model.Product;
import com.casaca.product.domain.repository.RecommendedProductRepository;
import com.casaca.product.infrastructure.rest.api.product.get.ProductDetailApiResponse;
import com.casaca.product.infrastructure.rest.api.product.get.ProductResponseApiMapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

public class RecommendedProductApi implements RecommendedProductRepository {

	public static final String SIMILAR_PRODUCTS = "/product/{productId}/similarids";

	public static final String PRODUCT_ID = "/product/{productId}";
	private final RestTemplate restTemplate;
	private final SimilarProductsCache similarProductsCache;
	private final ExecutorService executorService;
	private final ProductResponseApiMapper productResponseApiMapper;
	@Value("${external.api.base.url}")
	private String externalApiBaseUrl;

	public RecommendedProductApi(
		SimilarProductsCache similarProductsCache,
		ProductResponseApiMapper productResponseApiMapper
	) {
		this.productResponseApiMapper = productResponseApiMapper;
		this.restTemplate = new RestTemplate();
		this.similarProductsCache = similarProductsCache;
		this.executorService = Executors.newFixedThreadPool(10);
	}

	public List<Product> getProducts(String productId) {
		String url = externalApiBaseUrl + SIMILAR_PRODUCTS.replace("{productId}", productId);

		CompletableFuture<List<Product>> future = CompletableFuture.supplyAsync(() -> {
			String[] similarIds = Objects.requireNonNull(restTemplate.getForObject(
				url,
				String[].class
			));

			List<Product> similarProducts = new ArrayList<>();
			for(String id : similarIds) {
				this.getProductById(id).ifPresent(similarProducts::add);
			}

			return similarProducts;
		}, executorService);

		try {
			return future.get(200, TimeUnit.MILLISECONDS);
		} catch(InterruptedException | ExecutionException e) {
			throw new NotFoundSimilarProductsException(
				String.format("There are no recommended products for product id: %s", productId)
			);
		} catch(TimeoutException e) {
			future.thenAcceptAsync(
				similarProducts -> similarProductsCache.put(productId, similarProducts),
				executorService
			);
			return Collections.emptyList();
		}
	}

	private Optional<Product> getProductById(String productId) {
		try {
			String url = externalApiBaseUrl + PRODUCT_ID.replace("{productId}", productId);
			return Optional.ofNullable(this.productResponseApiMapper.map(Objects.requireNonNull(restTemplate.getForObject(
				url,
				ProductDetailApiResponse.class
			))));
		} catch(HttpStatusCodeException ignored) {
			return Optional.empty();
		}
	}
}
