package com.casaca.product.infrastructure.rest.controller.product;

import com.casaca.product.application.service.product.RecommendedProductsService;
import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetail;
import com.casaca.product.infrastructure.rest.controller.product.response.ProductsDetailResponseMapper;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductApiController implements ProductApi {

	private final RecommendedProductsService recommendedProductsService;
	private final ProductsDetailResponseMapper productsDetailResponseMapper;

	@GetMapping(value = "/{productId}/similar", produces = {"application/json"})
	public ResponseEntity<List<ProductDetail>> getProductSimilar(@Valid @PathVariable("productId") String productId) {
		return ResponseEntity.ok(this.productsDetailResponseMapper.map(this.recommendedProductsService.execute(productId)));
	}

}
