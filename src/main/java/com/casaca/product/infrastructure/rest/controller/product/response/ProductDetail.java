package com.casaca.product.infrastructure.rest.controller.product.response;

import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProductDetail {
	@Schema(description = "product ID", example = "100")
	private String id;
	@Schema(description = "product name", example = "Jacket")
	private String name;
	@Schema(description = "price of product", example = "20.20")
	private BigDecimal price;
	@Schema(description = "Product availability", example = "true")
	private Boolean availability;
}
