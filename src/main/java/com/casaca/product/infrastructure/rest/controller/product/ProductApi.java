package com.casaca.product.infrastructure.rest.controller.product;


import com.casaca.product.infrastructure.rest.controller.product.response.ProductDetail;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;


public interface ProductApi {

	@Operation(
		summary = "Similar products",
		description = "Endpoint that, given a product, returns similar products ",
		operationId = "getProductSimilar",
		tags = "product api")
	@ApiResponses(value = {
		@ApiResponse(responseCode = "200", description = "Similar products recovered"),
		@ApiResponse(responseCode = "404", description = "Product Not found")})
	ResponseEntity<List<ProductDetail>> getProductSimilar(
		@Parameter(in = ParameterIn.PATH, description = "Id of the product of which we want the similar ones",example = "2",
			required = true, name = "productId") @PathVariable("productId") String productId
	);

}

