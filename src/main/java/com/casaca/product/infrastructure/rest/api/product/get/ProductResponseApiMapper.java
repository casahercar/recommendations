package com.casaca.product.infrastructure.rest.api.product.get;

import com.casaca.product.domain.model.Product;

public class ProductResponseApiMapper {

	public Product map(ProductDetailApiResponse productDetailApiResponse) {
		Product product = new Product();
		product.setId(productDetailApiResponse.getId());
		product.setName(productDetailApiResponse.getName());
		product.setAvailability(productDetailApiResponse.getAvailability());
		product.setPrice(productDetailApiResponse.getPrice());
		return product;
	}

}
