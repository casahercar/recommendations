package com.casaca.product.infrastructure.rest.api.product.get;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ProductDetailApiResponse {

	private String id;
	private String name;
	private BigDecimal price;
	private Boolean availability;

}
