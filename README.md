## Product Service

## Description
A service in which we can consult on the basis of a product, all similar products. The service is fault-tolerant for the recommended products. When a product cannot be recovered then the rest of the recommended products will be returned. If there are no recommended products for that product, then no product will be returned, but there will not be a failure in the application.

## 🛠️ Install and start the project
- The current release of Product Service OpenJDK Java supports Java 11
- mvn spring-boot:run
- The localhost port is  5000

  <h3>External infrastructure</h3>

- influxdb
- Grafana
- A service that returns a Json with Mocks
- K6

## Usage
- Given a product when querying the external api with that product, then the recommended products for that product are returned to the user.
- For the correct local operation of this application we need an external Mock infrastructure.
- It contains an integration test that tests all the cases.
  An external api call with would be: \
http://localhost:5000/1/similar

<pre>Response JSON
[
  {
    "id": "2",
    "name": "Dress",
    "price": 19.99,
    "availability": true
  },
  {
    "id": "3",
    "name": "Blazer",
    "price": 29.99,
    "availability": false
  },
  {
    "id": "4",
    "name": "Boots",
    "price": 39.99,
    "availability": true
  }
]
</pre>

<h3>Swagger Documentation</h3>
http://localhost:5000/swagger-ui/index.html

## Author
Carlos Hernández Casares